import {
  View,
  Text,
  SafeAreaView,
  Platform,
  TextInput,
  Image,
  StyleSheet,
  ScrollView,
} from 'react-native';
import React from 'react';
import iconFood from './src/icon/food.png';

const App = () => {
  return (
    <SafeAreaView style={style.atas}>
      <View>
        <View style={style.boxSrch}>
          <Image
            style={{
              width: 25,
              height: 25,
              top: 10,
              resizeMode: 'contain',
              right: 10,
            }}
            source={{
              uri: 'https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/search-512.png',
            }}></Image>
          <TextInput
            placeholder="Search"
            style={{left: 25, bottom: Platform.OS == 'ios' ? 8 : 27}}
          />
        </View>
        <View style={style.boxScan}>
          <Image
            style={{
              width: 40,
              height: 40,
              top: 4.5,
              resizeMode: 'contain',
              right: 11,
            }}
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwdAz8Wa7BbOT9uf2bza5MUZvvtsG8pfu9fA&usqp=CAU',
            }}></Image>
        </View>
        <View
          style={{
            height: 70,
            bottom: 10,
            marginHorizontal: Platform.OS == 'ios' ? 17 : 15,
            right: Platform.OS == 'ios' ? 8 : null,
          }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={style.food}>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 3,
                  }}
                  source={iconFood}></Image>
                <Text style={{fontSize: 10, top: 7, left: 3.7}}>Makanan</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 17,
                  }}
                  source={{
                    uri: 'https://images.vexels.com/media/users/3/128277/isolated/preview/5f43f9bf3ee44e9c61c2e46b24827bf2-motor-bike-icon-silhouette.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 26}}>Motor</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 30,
                  }}
                  source={{
                    uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Circle-icons-car.svg/2048px-Circle-icons-car.svg.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 39}}>Mobil</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 46,
                  }}
                  source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/4003/4003833.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 53}}>Health</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 65,
                  }}
                  source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/679/679821.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 79}}>Box</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 90,
                  }}
                  source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/1915/1915314.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 102}}>Mart</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 115,
                  }}
                  source={{
                    uri: 'https://robotbiru.com/assets/images/home/rbi_pulsa_banner_icon.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 124}}>Pulsa</Text>
              </View>
              <View>
                <Image
                  style={{
                    width: 45,
                    height: 45,
                    top: 5,
                    resizeMode: 'contain',
                    left: 130,
                  }}
                  source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/895/895448.png',
                  }}></Image>
                <Text style={{fontSize: 10, top: 7, left: 139}}>Hotel</Text>
              </View>
            </View>
          </ScrollView>
        </View>
        <View
          style={{
            width: '100%',
            height: Platform.OS == 'ios' ? '70%' : '50%',
            flexDirection: 'row',
          }}>
          <View style={style.saldo}>
            <Text style={{color: 'black', left: 5, top: 5}}>Saldo</Text>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: 'black',
                  left: 5,
                  top: Platform.OS == 'ios' ? 47 : 37,
                  fontWeight: 'bold',
                }}>
                Rp 30.000
              </Text>
              <Image
                style={{
                  width: Platform.OS == 'ios' ? 25 : 33,
                  height: Platform.OS == 'ios' ? 25 : 33,
                  top: Platform.OS == 'ios' ? 43 : 30,
                  resizeMode: 'contain',
                  left: Platform.OS == 'ios' ? 65 : 75,
                }}
                source={{
                  uri: 'https://cdn-icons-png.flaticon.com/512/218/218390.png',
                }}></Image>
            </View>
          </View>
          <View style={style.point}>
            <Text style={{color: 'black', left: 5, top: 5}}>Point</Text>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: 'black',
                  left: 5,
                  top: Platform.OS == 'ios' ? 47 : 37,
                  fontWeight: 'bold',
                }}>
                452
              </Text>
              <Image
                style={{
                  width: Platform.OS == 'ios' ? 35 : 33,
                  height: Platform.OS == 'ios' ? 35 : 33,
                  top: Platform.OS == 'ios' ? 33 : 30,
                  resizeMode: 'contain',
                  left: Platform.OS == 'ios' ? 97 : 114,
                }}
                source={{
                  uri: 'https://cdn-icons-png.flaticon.com/512/5448/5448224.png',
                }}></Image>
            </View>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            height: Platform.OS == 'ios' ? '100%' : '90%',
            flexDirection: 'row',
            top: Platform.OS == 'ios' ? -55 : 10,
          }}>
          <View
            style={{
              width: '95%',
              left: 10,
              height: 240,
            }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <View style={style.promo}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{color: 'black', left: -1, top: 18}}>
                    Berhemat Sekarang
                  </Text>
                  <Image
                    style={{
                      width: Platform.OS == 'ios' ? 17 : 20,
                      height: Platform.OS == 'ios' ? 17 : 20,
                      top: Platform.OS == 'ios' ? 19.5 : 17,
                      resizeMode: 'contain',
                      left: Platform.OS == 'ios' ? 5 : 5,
                    }}
                    source={{
                      uri: 'https://static-00.iconduck.com/assets.00/arrow-right-circle-icon-512x512-2p1e2aaw.png',
                    }}></Image>
                </View>
                <View style={{flexDirection: 'row', top: 10}}>
                  <Image
                    style={{
                      width: Platform.OS == 'ios' ? 350 : '100%',
                      height: Platform.OS == 'ios' ? 250 : 250,
                      resizeMode: 'contain',
                      bottom: Platform.OS == 'ios' ? 10 : 5,
                    }}
                    source={{
                      uri: 'https://cdn-2.tstatic.net/pontianak/foto/bank/images/promo-grab-mulai-hari-ini-dapatkan-diskon-hingga-70-persen.jpg',
                    }}></Image>
                  <Image
                    style={{
                      width: Platform.OS == 'ios' ? 350 : '100%',
                      height: Platform.OS == 'ios' ? 250 : 250,
                      resizeMode: 'contain',
                      bottom: Platform.OS == 'ios' ? 10 : 5,
                      left: 10,
                    }}
                    source={{
                      uri: 'https://assets.grab.com/wp-content/uploads/sites/9/2019/12/13174801/GF-Jangan-Lupa-Makan-Signature-In-App-Banner-700x300-700x300.jpg',
                    }}></Image>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: Platform.OS == 'ios' ? '100%' : '90%',
            flexDirection: 'row',
            top: Platform.OS == 'ios' ? -90 : 25,
          }}>
          <View
            style={{
              width: '95%',
              left: 10,
              height: 240,
            }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <View style={style.promo}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{color: 'black', left: -1, top: 18}}>
                    Restoran Yang Kamu Sukai
                  </Text>
                  <Image
                    style={{
                      width: Platform.OS == 'ios' ? 17 : 20,
                      height: Platform.OS == 'ios' ? 17 : 20,
                      top: Platform.OS == 'ios' ? 19.5 : 17,
                      resizeMode: 'contain',
                      left: Platform.OS == 'ios' ? 5 : 5,
                    }}
                    source={{
                      uri: 'https://static-00.iconduck.com/assets.00/arrow-right-circle-icon-512x512-2p1e2aaw.png',
                    }}></Image>
                </View>
                <View style={{flexDirection: 'row', top: 10}}>
                  <Image
                    style={{
                      width: Platform.OS == 'ios' ? 350 : '100%',
                      height: Platform.OS == 'ios' ? 250 : 250,
                      resizeMode: 'contain',
                      bottom: Platform.OS == 'ios' ? 10 : 5,
                    }}
                    source={{
                      uri: 'https://cdn-2.tstatic.net/pontianak/foto/bank/images/promo-grab-mulai-hari-ini-dapatkan-diskon-hingga-70-persen.jpg',
                    }}></Image>
                  <Image
                    style={{
                      width: Platform.OS == 'ios' ? 350 : '100%',
                      height: Platform.OS == 'ios' ? 250 : 250,
                      resizeMode: 'contain',
                      bottom: Platform.OS == 'ios' ? 10 : 5,
                      left: 10,
                    }}
                    source={{
                      uri: 'https://assets.grab.com/wp-content/uploads/sites/9/2019/12/13174801/GF-Jangan-Lupa-Makan-Signature-In-App-Banner-700x300-700x300.jpg',
                    }}></Image>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  atas: {
    height: Platform.OS == 'ios' ? 100 : 40,
    backgroundColor: 'green',
  },
  boxSrch: {
    backgroundColor: 'white',
    height: 50,
    top: Platform.OS == 'ios' ? 15 : 15,
    elevation: 5,
    borderRadius: 8,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    borderRightWidth: 0,
    marginHorizontal: 65,
    borderColor: 'gray',
    borderWidth: 0.3,
    paddingHorizontal: 15,
    right: 20,
  },
  boxScan: {
    backgroundColor: 'white',
    height: 50,
    left: Platform.OS == 'ios' ? 307 : 326.5,
    elevation: 5,
    borderRadius: 8,
    width: 50,
    borderColor: 'grey',
    borderRightWidth: 0.3,
    borderTopWidth: 0.3,
    borderLeftWidth: Platform.OS == 'ios' ? 0.2 : 0.2,
    borderBottomWidth: 0.3,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    paddingHorizontal: 15,
    bottom: Platform.OS == 'ios' ? 35 : 35,
  },
  food: {
    flexDirection: 'row',
    width: 500,
    height: 50,
    alignItems: 'center',
    borderRadius: 100,
  },
  saldo: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    width: Platform.OS == 'ios' ? '43%' : '43%',
    height: Platform.OS == 'ios' ? '50%' : '85%',
    top: 15,
    left: Platform.OS == 'ios' ? 10 : 15,
    borderRadius: 8,
  },
  point: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    width: Platform.OS == 'ios' ? '43%' : '43%',
    height: Platform.OS == 'ios' ? '50%' : '85%',
    top: 15,
    left: Platform.OS == 'ios' ? 30 : 45,
    borderRadius: 8,
  },
  promo: {
    width: Platform.OS == 'ios' ? '100%' : 365,
    top: Platform.OS == 'ios' ? -20 : -15,
    left: Platform.OS == 'ios' ? 10 : 15,
    borderRadius: 8,
  },
});
export default App;
