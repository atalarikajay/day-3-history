// import {View, Text, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
// import React, {useEffect, useState} from 'react';
// import Icon from 'react-native-vector-icons/FontAwesome5';

// const App = () => {
//   const [datas, setDatas] = useState(null);
//   const getData = () => {
//     var requestOptions = {
//       method: 'GET',
//       body: datas,
//       redirect: 'follow',
//     };

//     fetch(
//       'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
//       requestOptions,
//     )
//       .then(response => response.text())
//       .then(result => {
//         // console.log(result)
//         const parsing = JSON.parse(result);
//         setDatas(Object.entries(parsing));
//       })
//       .catch(error => console.log('error', error));
//   };
//   useEffect(() => {
//     getData();
//   }, []);

//   return (
//     <SafeAreaView style={style.container}>
//       <ScrollView contentContainerStyle={{padding: 20}}>
//         <View style={style.headerBar}>
//           <Text style={style.txtBar}>History</Text>
//         </View>
//         {datas?.map((e: any) => {
//           return (
//             <View
//               style={{
//                 backgroundColor: 'white',
//                 width: '100%',
//                 elevation: 5,
//                 shadowColor: 'black',
//                 shadowOpacity: 0.2,
//                 shadowRadius: 4,
//                 shadowOffset: {
//                   height: 1,
//                   width: 2,
//                 },
//                 paddingVertical: 15,
//                 marginBottom: 15,
//                 borderRadius: 10,
//               }}>
//               <View
//                 style={{
//                   flexDirection: 'row',
//                 }}>
//                 <Icon
//                   name="envelope"
//                   size={30}
//                   color="#900"
//                   style={{marginTop: 7, marginLeft: 3}}
//                 />
//                 <View style={{paddingLeft: 15}}>
//                   <Text style={{fontWeight: 'bold'}}>
//                     Sender : {e[1].sender}
//                   </Text>
//                   <Text>Jumlah : Rp. {e[1].amount}</Text>
//                   <Text style={{fontFamily: 'ShantellSans-Bold'}}>
//                     Metode : {e[1].type}
//                   </Text>
//                 </View>
//               </View>
//             </View>
//           );
//         })}
//       </ScrollView>
//     </SafeAreaView>
//   );
// };
// const style = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: 'grey',
//     justifyContent: 'center',
//   },
//   itemList: {
//     paddingVertical: 15,
//   },
//   headerBar: {
//     backgroundColor: 'cyan',
//     padding: 20,
//     marginBottom: 15,
//     borderRadius: 7,
//   },
//   txtBar: {
//     fontSize: 20,
//     fontWeight: 'bold',
//   },
// });
// export default App;

// import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
// import React from 'react';

// const App = () => {
//   return (
//     <SafeAreaView
//       style={{
//         flex: 1,
//       }}>
//       <View style={{flex: 1, backgroundColor: 'red'}}></View>
//       <View style={{flex: 2, backgroundColor: 'darkorange'}} />
//       <View style={{flex: 3, backgroundColor: 'green'}} />
//     </SafeAreaView>
//   );
// };
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     padding: 20,
//   },
// });

// export default App;

// import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
// import React from 'react';

// const App = () => {
//   return (
//     <SafeAreaView style={style.container}>
//       <View style={style.kotak}>
//         <View style={style.circle}></View>
//         <View style={style.circle1}></View>
//         <View style={style.circle2}>
//           <View style={style.circle3}></View>
//           <View style={style.circle4}></View>
//           <View style={style.circle5}></View>
//         </View>
//       </View>
//     </SafeAreaView>
//   );
// };
// const style = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   kotak: {
//     backgroundColor: 'white',
//     justifyContent: 'center',
//     alignItems: 'center',
//     width: '65%',
//     height: '35%',
//     borderRadius: 10,
//     shadowRadius: 4,
//     shadowOpacity: 2,
//   },
//   circle: {
//     backgroundColor: 'green',
//     width: 200,
//     height: 200,
//     borderRadius: 100,
//     position: 'absolute',
//   },
//   circle1: {
//     backgroundColor: 'white',
//     width: 145,
//     height: 145,
//     borderRadius: 70,
//   },
//   circle2: {
//     backgroundColor: 'green',
//     width: 65,
//     height: 65,
//     borderRadius: 35,
//     position: 'absolute',
//   },
//   circle3: {
//     backgroundColor: 'white',
//     width: 35,
//     height: 35,
//     marginTop: 103,
//     left: 20,
//   },
//   circle4: {
//     backgroundColor: 'green',
//     width: 30.1,
//     height: 27,
//     borderRadius: 30,
//     top: 104,
//     left: -1,
//     position: 'absolute',
//   },
//   circle5: {
//     backgroundColor: 'green',
//     width: 30,
//     height: 26.2,
//     borderRadius: 15,
//     top: 103,
//     left: 42.5,
//     position: 'absolute',
//   },
// });

// export default App;

// import {
//   View,
//   Text,
//   SafeAreaView,
//   useColorScheme,
//   StyleSheet,
// } from 'react-native';
// import React from 'react';

// const App = () => {
//   const theme = useColorScheme();
//   console.log(theme, 'apaa dah');
//   return (
//     <SafeAreaView
//       style={[
//         //style.container(theme),
//         style.container,
//         {backgroundColor: theme == 'dark' ? 'black' : 'white'}, // styling dengna inline dengan array
//       ]}>
//       <View>
//         <Text>app</Text>
//       </View>
//     </SafeAreaView>
//   );
// };
// const style = StyleSheet.create({
//   //container: (theme: any) => ({  //ini untuk outline
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     //backgroundColor: theme == 'dark' ? 'black' : 'white', // ini untuk membedakan dark theme dan light theme
//   }, //outline dengan props
//   //styling dengan array
// });
// export default App;

// import {View, Text, SafeAreaView} from 'react-native';
// import React from 'react';

// const App = () => {
//   return (
//     <SafeAreaView style={{flex: 1, backgroundColor: 'red'}}>
//       <View>
//         <Text>App</Text>
//       </View>
//     </SafeAreaView>
//   );
// };

// export default App;
